[size=6][b][img]https://www.kdau.com/PortableTV/icon.png[/img] Portable TV[/b][/size]

[i]a [url=http://stardewvalley.net/]Stardew Valley[/url] mod by [url=https://www.kdau.com]kdau[/url][/i]

It's little! It's lovely! It shows Livin' Off The Land! The Portable TV is a craftable item carried in your inventory that lets you watch TV channels (standard and custom) from anywhere in Stardew Valley.

[img]https://www.kdau.com/headers/compatibility.png[/img]

[b]Game:[/b] Stardew Valley 1.5+

[b]Platform:[/b] Linux, macOS or Windows (Android: use 1.4 version; no TV on/off sounds)

[b]Multiplayer:[/b] works; every player must install

[b]Other mods:[/b] no known conflicts; custom channels will be listed with standard ones

[img]https://www.kdau.com/headers/installation.png[/img]

[list=1]
[*]Install [url=https://smapi.io/]SMAPI[/url]
[*]Install [url=https://www.nexusmods.com/stardewvalley/mods/1720]Json Assets[/url]
[*]Install [url=https://www.nexusmods.com/stardewvalley/mods/5098]Generic Mod Config Menu[/url] (optional, for easier configuration)
[*]Download this mod from the link in the header above
[*]Unzip the mod and place the [code]PortableTV[/code] and [code][JA]PortableTV[/code] folders inside your [code]Mods[/code] folder
[*]Run the game using SMAPI
[/list]
[img]https://www.kdau.com/headers/use.png[/img]

The Portable TV is a resource item that can be carried in your inventory. Its crafting recipe is available immediately and requires a Gold Bar, 2 Refined Quartz and a Battery Pack.

To use the TV on Linux, macOS or Windows, select it in your inventory and right-click in an open part of the world. You can press "R" on your keyboard (or another key that you configure; see below) to activate any Portable TV in your inventory.

To use the TV on Android, select it in your inventory and tap anywhere in the world. You can also configure the Virtual Keyboard mod to add an "R" button; tapping it will activate any Portable TV in your inventory.

The farmer will pick up the TV and turn it on, showing the familiar channel list. After you have viewed your chosen program, the farmer will turn off the TV and put it away. Just use it again to watch something else.

[img]https://www.kdau.com/headers/configuration.png[/img]

If you have installed Generic Mod Config Menu, you can access this mod's configuration by clicking the cogwheel button at the lower left corner of the Stardew Valley title screen and then choosing "Portable TV".

Otherwise, you can edit this mod's [code]config.json[/code] file. It will be created in the mod's main folder ([code]Mods/PortableTV[/code]) the first time you run the game with the mod installed. These options are available:

[list]
[*][code]Animate[/code]: Set this to [code]false[/code] to have the Portable TV appear and disappear instantly from the screen.
[*][code]Static[/code]: Set this to [code]false[/code] to remove the light static from the Portable TV screen.
[*][code]Music[/code]: Set this to [code]false[/code] to prevent the Portable TV from playing music tracks over the standard channels. (Custom channels should be configured through their own mods.)
[*][code]ActivateKey[/code]: Set this to any valid keybinding that will then activate a Portable TV in your inventory. [url=https://stardewvalleywiki.com/Modding:Player_Guide/Key_Bindings#Available_bindings]See the list of keybindings here.[/url]
[/list]

[img]https://www.kdau.com/headers/translation.png[/img]

This mod can be translated into any language supported by Stardew Valley. It is currently available in English, Spanish, French and Hungarian, with further limited translations to German, Italian and Portuguese.

Your contribution would be welcome. Please see the [url=https://stardewvalleywiki.com/Modding:Translations]instructions on the wiki[/url]. You can send me your work in [url=https://gitlab.com/kdau/portabletv/-/issues]a GitLab issue[/url] or the Posts tab above.

[img]https://www.kdau.com/headers/acknowledgments.png[/img]

[list]
[*]Like all mods, this one is indebted to ConcernedApe, Pathoschild and the various framework modders.
[*]This mod is an implementation of an [url=https://github.com/StardewModders/mod-ideas/issues/268]idea from HoustoCo[/url].
[*]Coding of this mod relied on [url=https://www.nexusmods.com/stardewvalley/mods/1726]PyTK[/url] by Platonymous as a key example.
[*]The #making-mods channel on the [url=https://discordapp.com/invite/StardewValley]Stardew Valley Discord[/url] offered valuable guidance and feedback.
[*]The French translation was prepared by Inu'tile.
[*]The Hungarian translation was prepared by martin66789.
[*]The sound when the TV turns on is clipped from [url=https://freesound.org/people/pfranzen/sounds/328171/]Turning on an old CRT TV[/url] by [url=https://freesound.org/people/pfranzen/]pfranzen[/url], used under [url=http://creativecommons.org/licenses/by/3.0/]CC BY 3.0[/url].
[*]The sound when the TV turns off is [url=https://freesound.org/people/xtrgamr/sounds/321420/]Electric1.wav[/url] by [url=https://freesound.org/people/xtrgamr/]xtrgamr[/url], used under [url=http://creativecommons.org/licenses/by/3.0/]CC BY 3.0[/url].
[*]The visual static when choosing a channel is adapted from [url=https://commons.wikimedia.org/wiki/File:Random_static.gif]Random static.gif[/url] by [url=https://commons.wikimedia.org/wiki/User:Atomicdragon136]Atomicdragon136[/url], in the public domain.
[/list]

[img]https://www.kdau.com/headers/see-also.png[/img]

[list]
[*][url=https://gitlab.com/kdau/portabletv/-/blob/main/doc/RELEASE-NOTES.md]Release notes[/url]
[*][url=https://gitlab.com/kdau/portabletv]Source code[/url]
[*][url=https://gitlab.com/kdau/portabletv/-/issues]Report bugs[/url]
[*][url=https://www.kdau.com/stardew]My other Stardew stuff[/url]
[*]Mirrors: [b]Nexus[/b], [url=https://www.moddrop.com/stardew-valley/mods/761325-portable-tv]ModDrop[/url], [url=https://forums.stardewvalley.net/resources/portable-tv.52/]forums[/url]
[/list]
Other TV mods you may enjoy:

[list]
[*][img]https://www.kdau.com/PublicAccessTV/icon.png[/img] [url=https://www.nexusmods.com/stardewvalley/mods/5605]Public Access TV[/url] for the day's mining conditions, garbage loot, train schedules, rare events and more
[*][url=https://www.nexusmods.com/stardewvalley/mods/5485]Gardening with Hisame[/url] for farm beautification tips
[/list]
