# Release notes

[Portable TV](../)

## 2.0.2

*Release pending.*

* Fix error message when returning to title screen

## 2.0.1

*Released 2021 June 26.*

* Fix missing files

## 2.0.0

*Released 2021 June 26.*

* Require, and fix compatibility with, Stardew 1.5 or higher
* Play music for a new channel and a secret one
* Fix layout size of a secret channel
* Don't require the Portable TV for the Full Shipping achievement
* Add Hungarian translation by martin66789

## 1.2.0

*Released 2020 April 28.*

* Also activate Portable TV with configurable keybinding
* On Android, look for taps in world instead of double-taps on inventory item
* Add `reset_portable_tv` console command
* Add French translation by Inu'tile

## 1.1.0

*Released 2020 April 3.*

* Support Android platform
* Support Generic Mod Config Menu framework
* Rearrange config options (`Static` now separate from `Animate`)
* Add Spanish translation by self
* Add other limited translations

## 1.0.0

*Released 2020 March 27.*

* Initial version
